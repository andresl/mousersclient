#include "cmove.h"

CMove::CMove( ) {
    this->coord = QPair< int, int > ( 0, 0 );
    this->movType = CMove::NONE;
}

CMove::CMove( int x, int y ) {
    this->coord = QPair< int, int > ( x, y );
    this->movType = CMove::NONE;
}

CMove::CMove( CMove::MOV_TYPE m, int x, int y ) {
    this->coord = QPair< int, int > ( x, y );
    this->movType = m;
}

CMove::CMove( CMove::MOV_TYPE m, QPair< int, int > p ) {
    this->coord = p;
    this->movType = m;
}

CMove::~CMove( ) {
    // nothing for now
}

CMove::MOV_TYPE CMove::getMovType( ) {
    return this->movType;
}

void CMove::setMovType( CMove::MOV_TYPE m ) {
    this->movType = m;
}

void CMove::setCoord( int x, int y ) {
    this->coord = QPair< int, int >( x, y );
}

QPair< int, int > CMove::getCoord( ) {
    return this->coord;
}

