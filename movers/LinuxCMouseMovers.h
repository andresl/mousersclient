#ifndef LINUXCMOUSEMOVERS_H
#define LINUXCMOUSEMOVERS_H

#include "CMouseMovers.h"
#include <vector>
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#include <unistd.h>

using namespace std;

class LinuxCMouseMovers : public CMouseMovers {
public:
    void leftClick( );
    void rightClick( );
    int queryX( );
    int queryY( );
    void setPos( int, int );
    void doSleep( unsigned int );
};

#endif // LINUXCMOUSEMOVERS_H
