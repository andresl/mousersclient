#include "WindowsCMouseMovers.h"

using namespace std;

void WindowsCMouseMovers::setPos( int x, int y ) {
//    double fScreenWidth    = ::GetSystemMetrics( SM_CXSCREEN )-1;
//    double fScreenHeight  = ::GetSystemMetrics( SM_CYSCREEN )-1;
//    double fx = x*(65535.0f/fScreenWidth);
//    double fy = y*(65535.0f/fScreenHeight);
//    INPUT  Input={0};
//    Input.type      = INPUT_MOUSE;
//    Input.mi.dwFlags  = MOUSEEVENTF_MOVE|MOUSEEVENTF_ABSOLUTE;
//    Input.mi.dx = fx;
//    Input.mi.dy = fy;
//    ::SendInput(1,&Input,sizeof(INPUT));
    SetCursorPos( x, y );
}

void WindowsCMouseMovers::leftClick( ) {
    INPUT Input = { 0 };

    Input.type      = INPUT_MOUSE;
    Input.mi.dwFlags  = MOUSEEVENTF_LEFTDOWN;
    ::SendInput(1,&Input,sizeof(INPUT));

    // left up
    ::ZeroMemory(&Input,sizeof(INPUT));
    Input.type      = INPUT_MOUSE;
    Input.mi.dwFlags  = MOUSEEVENTF_LEFTUP;
    ::SendInput(1,&Input,sizeof(INPUT));
}

void WindowsCMouseMovers::rightClick( ) {
    INPUT Input = { 0 };

    Input.type      = INPUT_MOUSE;
    Input.mi.dwFlags  = MOUSEEVENTF_RIGHTDOWN;
    ::SendInput(1,&Input,sizeof(INPUT));

    // left up
    ::ZeroMemory(&Input,sizeof(INPUT));
    Input.type      = INPUT_MOUSE;
    Input.mi.dwFlags  = MOUSEEVENTF_RIGHTUP;
    ::SendInput(1,&Input,sizeof(INPUT));
}

int WindowsCMouseMovers::queryX( ) {
    POINT pt;
    GetCursorPos( &pt );
    return pt.x;
}

int WindowsCMouseMovers::queryY( ) {
    POINT pt;
    GetCursorPos( &pt );
    return pt.y;
}

void WindowsCMouseMovers::doSleep( unsigned int s1 ) {
   // usleep( sl );
    Sleep( s1 );
}
