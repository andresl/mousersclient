#include "MoversFactory.h"

#ifdef __gnu_linux__
#include "LinuxMoversFactory.h"
#endif

#ifdef _WIN32
#include "WindowsMoversFactory.h"
#endif

#ifdef __APPLE__ && __MACH__
#include "MacOSXMoversFactory.h"
#endif


MoversFactory* MoversFactory::createFactory( int type ) {
    MoversFactory *moversFactory = NULL;
    switch ( type ) {
#ifdef __gnu_linux__
      case LINUX:
        moversFactory = new LinuxMoversFactory( );
        break;
#endif

#ifdef _WIN32
      case WINDOWS:
        moversFactory = new WindowsMoversFactory( );
        break;
#endif

#ifdef __APPLE__ && __MACH__
      case MACOSX:
        moversFactory = new MacOSXMoversFactory( );
        break;
#endif

//    default:
//        moversFactory = NULL;
    }

    return moversFactory;
}
