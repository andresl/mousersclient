#ifndef WINDOWSMOVERSFACTORY_H
#define WINDOWSMOVERSFACTORY_H
#include "WindowsCMouseMovers.h"
#include "MoversFactory.h"



class WindowsMoversFactory: public MoversFactory {
public:
  CMouseMovers *getMoversCMouse( );
 // CKeyboardMovers *getMoversCKeyboard( );

};

#endif // WINDOWSMOVERSFACTORY_H
