#ifndef WINDOWSCMOUSEMOVERS_H
#define WINDOWSCMOUSEMOVERS_H
#include "CMouseMovers.h"
#include <Windows.h>

class WindowsCMouseMovers: public CMouseMovers {
public:
    void leftClick( );
    void rightClick( );
    int queryX( );
    int queryY( );
    void setPos( int, int );
    void doSleep( unsigned int );
};
#endif // WINDOWSCMOUSEMOVERS_H
