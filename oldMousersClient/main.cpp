#include <QApplication>
#include "moversservice/CMouseService.h"
#include "view/PrincipalWidget.h"
#include <QDebug>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    PrincipalWidget *pw = new PrincipalWidget( );
    pw->show( );

    return ( a.exec() );
}
