#-------------------------------------------------
#
# Project created by QtCreator 2015-06-02T15:51:58
#
#-------------------------------------------------

QT       += core gui  widgets network

TARGET = mousers-client
CONFIG   += console
CONFIG   -= app_bundle
TEMPLATE = app

SOURCES += main.cpp \
    movers/MoversFactory.cpp \
    movers/CMouseMovers.cpp \
    moversservice/CMouseService.cpp \
    view/PrincipalWidget.cpp \

HEADERS += \
    movers/MoversFactory.h \
    movers/CMouseMovers.h \
    moversservice/CMouseService.h \
    view/PrincipalWidget.h \

linux-g++ {
    LIBS += -lXtst -lX11

    HEADERS += movers/LinuxMoversFactory.h \
               movers/LinuxCMouseMovers.h \

    SOURCES += movers/LinuxMoversFactory.cpp \
               movers/LinuxCMouseMovers.cpp \
}

macx {
    # gcc -o program program.c -Wall -framework ApplicationServices
    # LIBS += ???

    HEADERS += MacOSXMoversFactory.h \
               MacOSXCMouseMovers.h \
               
    SOURCES += MacOSXMoversFactory.cpp \
               MacOSXCMouseMovers.cpp \
}

win32 {
    HEADERS += WindowsMoversFactory.h \
               WindowsCMouseMovers.h \ 

    SOURCES += WindowsMoversFactory.cpp \
               WindowsCMouseMovers.cpp \	
}
