
#include <iostream>
#include "CMouseService.h"
#include "../movers/MoversFactory.h"
#include <unistd.h>

using namespace std;

CMouseService* CMouseService::ptr_instancia = NULL;

CMouseService* CMouseService::devuelveInstancia( ) {
  if ( !ptr_instancia ) {
    ptr_instancia = new CMouseService( );

  }

  return ptr_instancia;

}

void CMouseService::doSomething( ) {
  cout << "Do something...!" << endl;
}

void CMouseService::moveToPosition( int x, int y ) {
  MoversFactory *movers_factory = MoversFactory::createFactory( CMouseService::OS_TYPE );
  CMouseMovers *mouse_movers = movers_factory->getMoversCMouse( );
  mouse_movers->setPos( x, y );

}
void CMouseService::moveToPositionGradually( int x, int y) {
    unsigned int sl = 1000;
    this->moveToPositionGradually( x, y, sl );
}

void CMouseService::moveToPositionGradually( int x, int y, unsigned int sl ) {
  int step = 1;

  MoversFactory *mf = MoversFactory::createFactory( CMouseService::OS_TYPE );
  CMouseMovers *mm = mf->getMoversCMouse( );
  int currentX = mm->queryX( );
  int currentY = mm->queryY( );
  double slope = ( double ) ( y - currentY ) / ( x - currentX );
  double b = y - slope * x;
  double x0 = currentX, y0 = currentY;

  if ( currentX >= x ) {

    while ( x0 >= x ) {
      x0 = x0 - step;
      y0 = slope * x0 + b;
      usleep( sl );
      mm->setPos( x0, y0 );
    }

  } else {

    while ( x0 < x ) {
      x0 = x0 +step;
      y0 = slope * x0 + b;
      usleep( sl );
      mm->setPos( x0, y0 );
    }

  }



}

void CMouseService::doLeftClick( ) {
    MoversFactory *mf = MoversFactory::createFactory( CMouseService::OS_TYPE );
    CMouseMovers *mm = mf->getMoversCMouse( );
    mm->leftClick();

}

void CMouseService::doRightClick( ) {
    MoversFactory *mf = MoversFactory::createFactory( CMouseService::OS_TYPE );
    CMouseMovers *mm = mf->getMoversCMouse( );
    mm->rightClick();
}

int CMouseService::x( ) {
    MoversFactory *mf = MoversFactory::createFactory( CMouseService::OS_TYPE );
    CMouseMovers *mm = mf->getMoversCMouse( );
    return mm->queryX( );
}

int CMouseService::y( ) {
    MoversFactory *mf = MoversFactory::createFactory( CMouseService::OS_TYPE );
    CMouseMovers *mm = mf->getMoversCMouse( );
    return mm->queryY( );
}
