#ifndef CMOUSESERVICE_H
#define CMOUSESERVICE_H
#include "../movers/MoversFactory.h"
#include <iostream>
using namespace std;
class CMouseService {

private:
  static CMouseService *ptr_instancia;

public:
  static const int OS_TYPE = MoversFactory::LINUX; 

  static CMouseService *devuelveInstancia( );
  
  void doSomething( );
  
  void moveToPosition( int, int);

  void moveToPositionGradually( int, int, unsigned int );

  void moveToPositionGradually( int, int );

  void doLeftClick( );

  void doRightClick( );

  int x( );
  int y( );

};

#endif // CMOUSESERVICE_H
