#include "PrincipalWidget.h"
#include <QDebug>
#include <QTcpSocket>
#include <QString>
#include <unistd.h>
#include <QHostAddress>
#include "moversservice/CMouseService.h"

PrincipalWidget::PrincipalWidget( QWidget *parent ) : QWidget( parent ) {
    this->setWindowTitle( "Mousers Mouse Control Client" );
    // widgets
    this->lblEstado = new QLabel( "DESCONECTADO", this );
    this->txtHost =  new QLineEdit( this );
    this->txtPuerto = new QLineEdit( this );
    this->lblHost = new QLabel( "Host", this );
    this->lblPuerto = new QLabel( "Puerto", this );
    this->cmdConectar = new QPushButton( "Conectar", this );

    // layouts
    this->vl1 = new QVBoxLayout( this );
    this->vl2 = new QVBoxLayout( );
    this->vl3 = new QVBoxLayout( );
    this->hl = new QHBoxLayout( );

    // armando gui


    this->vl1->addWidget( this->lblEstado );
    this->vl1->addLayout( this->hl );
    this->vl1->addWidget( this->cmdConectar );

    this->hl->addLayout( this->vl2 );
    this->hl->addLayout( this->vl3 );

    this->vl2->addWidget( this->lblHost );
    this->vl2->addWidget( this->lblPuerto );

    this->vl3->addWidget( this->txtHost );
    this->vl3->addWidget( this->txtPuerto );





    // conectado senhales y ranuras
    connect( this->cmdConectar, SIGNAL( clicked( ) ), this, SLOT( conectarServidor( ) ) );

}

PrincipalWidget::~PrincipalWidget( ) {

}

void PrincipalWidget::conectarServidor( ) {

    //this->socketMousers->abort( );
    // socket
    this->socketMousers = new QTcpSocket( this );

    this->socketMousers->connectToHost( this->txtHost->text( ), this->txtPuerto->text( ).toInt( ) );

    if ( this->socketMousers->waitForConnected( 3000 ) ) {
        qDebug( ) << "Conectado";
        this->lblEstado->setText( "CONECTADO" );
    } else {
        qDebug( ) << "No se pudo conectar, hay algun servidor corriendo?";
        this->lblEstado->setText( "DESCONECTADO" );
    }

    connect( this->socketMousers, SIGNAL( readyRead() ), this, SLOT( leerMovimiento() ) );
}


void PrincipalWidget::leerMovimiento( ) {
    QByteArray buffer;

    CMouseService *ms = new CMouseService( );

    buffer.resize( this->socketMousers->bytesAvailable( ) );
    this->socketMousers->read( buffer.data( ), buffer.size( ) );
    qDebug( ) << ( QString ) buffer;
\
    QDataStream ds(buffer);
    int m = buffer.toInt( );

    qDebug() << "m:" << m;
    switch( m ) {
    case MOV_ARRIBA:
        ms->moveToPosition( ms->x( ), ms->y( ) - 10 );
        break;
    case MOV_ABAJO:
        ms->moveToPosition( ms->x( ), ms->y( ) + 10 );
        break;
    case MOV_DERECHA:
        ms->moveToPosition( ms->x( ) + 10, ms->y( ) );
        break;
    case MOV_IZQUIERDA:
        ms->moveToPosition( ms->x( ) - 10, ms->y( ) );
        break;

    case CLICK_IZQUIERDO:
        ms->doLeftClick( );
        break;
    case CLICK_DERECHO:
        ms->doRightClick( );
        break;

    }

    delete( ms );

}
