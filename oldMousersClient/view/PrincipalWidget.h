#ifndef PRINCIPALWIDGET_H
#define PRINCIPALWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QTcpSocket>

class PrincipalWidget : public QWidget
{
    Q_OBJECT
private:
    enum MovimientoRaton { MOV_ARRIBA, MOV_ABAJO, MOV_DERECHA, MOV_IZQUIERDA, CLICK_DERECHO, CLICK_IZQUIERDO };
    QLabel *lblEstado, *lblHost, *lblPuerto;

    QVBoxLayout *vl1, *vl2, *vl3;
    QHBoxLayout *hl;

    QLineEdit *txtHost, *txtPuerto;

    QPushButton *cmdConectar;
    QTcpSocket *socketMousers;



public:
    explicit PrincipalWidget(QWidget *parent = 0);
    ~PrincipalWidget();


signals:

public slots:
    void conectarServidor( );
    void leerMovimiento( );

};

#endif // PRINCIPALWIDGET_H
