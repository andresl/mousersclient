#ifndef LINUXCMOUSEMOVERS_H
#define LINUXCMOUSEMOVERS_H
#include <vector>
#include "CMouseMovers.h"
using namespace std;

class LinuxCMouseMovers : public CMouseMovers {
public:
    void leftClick( );
    void rightClick( );
    int queryX( );
    int queryY( );
    void setPos( int, int );
};

#endif // LINUXCMOUSEMOVERS_H
