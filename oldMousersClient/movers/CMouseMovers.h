#ifndef CMOUSEMOVERS_H
#define CMOUSEMOVERS_H
//include "CMouse.h"
#include <iostream>

using namespace std;
class CMouseMovers {
public:
    virtual void leftClick( ) = 0;
    virtual void rightClick( ) = 0;
    virtual int queryX( ) = 0;
    virtual int queryY( ) = 0;
    virtual void setPos( int, int ) = 0; 
};

#endif // CMOUSEMOVERS_H
