#include <QApplication>
#include "moversservice/CMouseService.h"
#include "view/PrincipalWidget.h"
#include <QDebug>
#include <QCursor>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    PrincipalWidget *pw = new PrincipalWidget( );
    pw->show( );
    QCursor *c = new QCursor;
    c->setPos( 1000, 1000 );
    return ( a.exec() );
}
