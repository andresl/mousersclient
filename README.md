# README #

This is Mousers client counterpart. Documentation in still in progress.

### What is this repository for? ###

* Quick summary

Source code in this repository is intended for deployment in desktop (Linux-based OS, Windows and Mac OS X). In order to get the server application, please refer to the [mousers](https://bitbucket.org/andresl/mousers) repo.

* Version

Mousers client is still in a pre-alpha stage. Code should build, but an unexpected action would lead to an application crash.


### How do I get set up? ###

Currently Linux-based OS running X11 for GUI is the only supported platform. In order to build the client you'll need X11 libs and Qt. Support for Windows and Mac OS X is in development and a stable version which supports those three main platforms is supposed to be released in August.